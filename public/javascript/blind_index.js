function createHash() {
    const intermediate = document.getElementById('sha256-ecms-bi-intermediate').value;

    const result = document.getElementById('sha256-ecms-bi-result-text');
    const resultDiv = document.getElementById('sha256-ecms-bi-result')

    const resultValError = document.getElementById('sha256-ecms-bi-val-error');
    resultDiv.style.display = 'none'
    resultValError.style.display = 'none'

    if (intermediate) {
        const hash = CryptoJS.SHA256(intermediate)
        result.innerHTML = `<div class="success">${hash}</div>`
        resultDiv.style.display = 'block'
        resultDiv.classList.add('govuk-notification-banner--success')    } else {
        resultValError.style.display = 'block'
    }
}

function createIntermediate() {
    const text = document.getElementById('sha256-ecms-bi-plaintext').value;
    const secret = document.getElementById('sha256-ecms-bi-secret').value;
    const intermediate = document.getElementById('sha256-ecms-bi-intermediate');
    intermediate.value = `${secret}${text.toUpperCase()}`

}


function createB64() {
    const text = document.getElementById('base64-plaintext').value;
    const result = document.getElementById('base64-result-text');
    const resultDiv = document.getElementById('base64-result')
    const resultValError = document.getElementById('base64-val-error');
    resultDiv.style.display = 'none'
    resultValError.style.display = 'none'


    if (text) {
        const hash = btoa(text)
        result.innerHTML = `<div class="success">${hash}</div>`
        resultDiv.style.display = 'block'
        resultDiv.classList.add('govuk-notification-banner--success')

    } else {
        resultValError.style.display = 'block'
    }
}

function decodeB64() {
    const text = document.getElementById('base64d-plaintext').value;
    const resultDiv = document.getElementById('base64d-result')
    const result = document.getElementById('base64d-result-text');
    const resultValError = document.getElementById('base64d-val-error');
    resultDiv.style.display = 'none'
    resultValError.style.display = 'none'

    if (text) {
        const hash = atob(text)
        resultDiv.style.display = 'block'
        resultDiv.classList.add('govuk-notification-banner--success')
        result.innerHTML = `Base64 successfully decoded <p class="code">${hash}</p></div>`
    } else {
        resultValError.style.display = 'block'
    }
}

